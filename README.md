# Garmin TCX 2 CSV Converter

**WARNING: This is a *dirty hack* I made to quickly convert a tcx/xml file to a csv file. Use it at your own risk!!!**

The tcx file can be exported from an activity on the garmin connect website using the small cog in the upper right corner as seen on this image:

![Export options](export.png)

I made a simple method to loop over the file line by line, when it detects a \<Trackpoint> tag, it will start parsing the time and heart rate values to extract the data and put in the csv output.

I made this parser specifically for my tcx file, and this code was intended as a one-time use only scenario. This method of parsing may not work for your export. If you decide to use this code anyway, then i cannot guarantee it will work for you.

# Installing & Running

Download the git repo:

`git clone git@gitlab.com:jakeobsen/garmin-tcx-2-csv-converter.git`

Setup the virtual environment:

`virtualenv garmin-tcx-2-csv-converter/`

Enter the folder and activate virtual environment:

`cd garmin-tcx-2-csv-converter && source bin/activate`

Make the converter into an executeable:

`chmod +x tcx2csv.py`

Run it:

`./tcx2csv.py -r 75 -m 196 -f activity.tcx | tee activity.csv`

Note: the program outputs the converted result to stdin, so we pipe it to `tee` and save to a new csv file.

# Command line options

```
Usage: tcx2csv.py [-h] -f FILE -r HREST -m HRMAX

Convert Garmin TCX file to CSV file

Optional arguments:
  -h, --help                Show help message and exit

Required arguments:
  -f FILE,  --file  FILE    File to be parsed
  -r HREST, --hrest HREST   Resting heartrate
  -m HRMAX, --hrmax HRMAX   Max heartrate
```

# License

BSD 2-Clause License

Copyright (c) 2018, Morten Jakobsen. All rights reserved.