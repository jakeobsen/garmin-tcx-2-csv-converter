#!/usr/bin/env python
# Copyright 2018 Morten Jakobsen. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import argparse
from re import findall

class garminTCX2CSV:
    def __init__(self):
        self.hrest = 0
        self.hrmax = 0

    def Karvonen(self, percentage):
        """
        Calculate karvonen heart rate at percentage

        :param percentage: Percentage used to calculate karvonen heartrate

        :return: Karvonen heartrate
        """
        hrr = self.hrmax - self.hrest
        return round(hrr * percentage + self.hrest)

    def whatZone(self, hr):
        """
        Return the current zone a given heartrate is at

        :param hr: Heartrate as BPM

        :return: Karvonen zonenumber
        """
        if   (hr >= self.Karvonen(0.5) and hr < self.Karvonen(0.6)) or hr < self.Karvonen(0.5):
            return 1
        elif (hr >= self.Karvonen(0.6) and hr < self.Karvonen(0.7)):
            return 2
        elif (hr >= self.Karvonen(0.7) and hr < self.Karvonen(0.8)):
            return 3
        elif (hr >= self.Karvonen(0.8) and hr < self.Karvonen(0.9)):
            return 4
        elif (hr >= self.Karvonen(0.9) and hr < self.Karvonen(1.0)) or hr > self.Karvonen(1.0):
            return 5
        else:
            return 0

    def convert(self):
        """

        """
        with open(self.filename, 'r') as f:
            xmlFile = f.readlines()
        xmlFile = [x.strip() for x in xmlFile] # Strip whitespace

        previousTime = 1
        time = 0
        hr = 0
        parseNext = 0
        print('timestamp,heartrate,zone')
        for line in xmlFile:
            if line == '<Trackpoint>':
                parseNext = 9

            if parseNext == 8:
                time = findall("<Time>(.*?)</Time>", line).pop()

            if parseNext == 5:
                hr = findall("<Value>(.*?)</Value>", line).pop()

            parseNext = parseNext - 1
            
            if(time != previousTime):

                if hr != 0:
                    print('{},{},{}'.format(time, hr, self.whatZone(int(hr))))
            
            previousTime = time

if __name__ == "__main__":
    # Grab arguments
    parser = argparse.ArgumentParser(description="Convert Garmin TCX file to CSV file")
    parser.add_argument('-f', '--file', type=str, help="File to be parsed", required=True)
    parser.add_argument('-r', '--hrest', type=int, help="Resting heartrate", required=True)
    parser.add_argument('-m', '--hrmax', type=int, help="Max heartrate", required=True)
    args = parser.parse_args()

    # Run program
    c = garminTCX2CSV()
    c.hrest = args.hrest
    c.hrmax = args.hrmax
    c.filename = args.file
    c.convert()